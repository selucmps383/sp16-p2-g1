﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phase2WebAPI.Models
{
    public class Product
    {
        [Required]
        [Column(TypeName = "varchar")]
        [Index(IsUnique = true)]
        [StringLength(512, ErrorMessage = "Product name can be no more than 512 characters")]
        public string Name { get; set; }

        // double check this var type
        // String or something else? (DateTime maybe?)
        [Column(TypeName = "varchar")]
        public string CreatedDate { get; set; }

        // double check this var type
        // String or something else? (DateTime maybe?)
        [Column(TypeName = "varchar")]
        public string LastModifiedDate { get; set; }

        [Range(0, Int32.MaxValue)]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        //double check this
        public int InventoryCount { get; set; }

        [Column(TypeName = "varchar")]
        public string Category { get; set; }

        [Column(TypeName = "varchar")]
        public string Manufacturer { get; set; }

        public virtual List<Category> Categorys { get; set;}
    }
}
