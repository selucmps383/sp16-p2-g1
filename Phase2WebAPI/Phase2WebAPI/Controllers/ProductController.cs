﻿using Phase2WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phase2WebAPI.Controllers
{
    public class ProductController : ApiController
    {
        [Route("Product/Index")]
        public HttpResponseMessage GetProducts()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new Product());
        }
    }
}
