﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Phase2WebAPI.Models
{
    public class User
    {
        
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [Index(IsUnique = true)]
        [DisplayName("Email")]
        [StringLength(512, ErrorMessage = "Email Address can be no more than 512 characters")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Column(TypeName = "varchar")]
        [MinLength(8, ErrorMessage = "Password must be more than 7 characters")]
        [StringLength(512, ErrorMessage = "Password can be no more than 512 characters")]
        public string Password { get; set; }


        [Column(TypeName ="varchar")]
        public string ApiKey { get; set; }


    }
}