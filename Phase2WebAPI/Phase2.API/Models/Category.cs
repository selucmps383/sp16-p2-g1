﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phase2WebAPI.Models
{
    public class Category
    {
        // Double check this
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public virtual List<Product> Product {get; set;}
    }
}
