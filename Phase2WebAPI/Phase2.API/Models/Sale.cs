﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phase2WebAPI.Models
{
    public class Sale
    {
        public int Id { get; set; }
        // DateTime or String?
        public DateTime Date { get; set; }

        public virtual List<Product> Product { get; set; }

        [Range(0, Int32.MaxValue)]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }

        [Column(TypeName = "varchar")]
        public string EmailAddress { get; set; }
    }
}
