﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phase2WebAPI.Models
{
    public class Product
    {
        
        public int ProductId { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [Index(IsUnique = true)]
        [StringLength(512, ErrorMessage = "Product name can be no more than 512 characters")]
        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        [Range(0, Int32.MaxValue)]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        public int InventoryCount { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public int ManufacturerId { get; set; }

        //[Required]
        public virtual Category Category { get; set;}

        public virtual Manufacturer Manufacturer { get; set; }

        public virtual List<Sale> Sale { get; set; }
    }
}
