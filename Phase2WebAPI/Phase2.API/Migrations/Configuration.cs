namespace Phase2.API.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Phase2.API.Models;
    using Phase2WebAPI.Models;
    using System.Data.Entity.Validation;
    using System.Text;
    using System.Web.Helpers;
    internal sealed class Configuration : DbMigrationsConfiguration<Phase2.API.Models.Phase2APIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Phase2.API.Models.Phase2APIContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Users.AddOrUpdate(
                x => x.Id,
                new User() { Id = 1,  Email = "sa@383.com", Password = Crypto.HashPassword("password"), ApiKey = null},
                new User() { Id = 2,  Email = "admin", Password = Crypto.HashPassword("adminadmin"), ApiKey = null}
            );


            context.Categories.AddOrUpdate(
                x => x.CategoryId,
                new Category() { CategoryId = 1, CategoryName = "Appliances" },
                new Category() { CategoryId = 2, CategoryName = "Gaming" }
            );



            context.Manufacturers.AddOrUpdate(
                x => x.ManufacturerId,
                new Manufacturer() { ManufacturerId = 1, ManufacturerName = "Amazon" },
                new Manufacturer() { ManufacturerId = 2, ManufacturerName = "Google" }

            );



            context.Products.AddOrUpdate(
                x => x.ProductId,
                new Product() { ProductId = 1, Name = "PaperCups", CreatedDate = DateTime.Today, LastModifiedDate = DateTime.Today, Price = 3, InventoryCount = 2, CategoryId = 1, ManufacturerId = 1 },
                new Product() { ProductId = 2, Name = "GTX 980", CreatedDate = DateTime.Today, LastModifiedDate = DateTime.Today, Price = 350, InventoryCount = 72, CategoryId = 2, ManufacturerId = 2}
            );

            context.Sales.AddOrUpdate(
                x => x.Id,
                new Sale() { Id = 1, EmailAddress = "admin@selu.edu", Date = DateTime.Today, TotalAmount = 25 }
            );


            context.SaveChanges();
            
        }
    }
}
