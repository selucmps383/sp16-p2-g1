﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phase2WebAPI.Models;
using Phase2.API.Models;
using System.Web.Helpers;
using System.Web.Security;

namespace Phase2.API.Controllers
{
    public class LoginController : Controller
    {

        private Phase2APIContext db = new Phase2APIContext();


        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Phase2WebAPI.Models.User User)
        {

            if (IsValid(User.Email, User.Password))
            {
                User currentUser = db.Users.FirstOrDefault(u => u.Email == User.Email);
                if (currentUser != null) Session["CurrentUserId"] = currentUser.Id;

                FormsAuthentication.SetAuthCookie(User.Email, true);
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Login information is incorrect.");
            return View(User);
        }

        [Authorize]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        private bool IsValid(string userName, string password)
        {
            bool IsValid = false;

            var user = db.Users.FirstOrDefault(u => u.Email.Equals(userName));

            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                IsValid = true;
            }
            return IsValid;
        }
    }
}