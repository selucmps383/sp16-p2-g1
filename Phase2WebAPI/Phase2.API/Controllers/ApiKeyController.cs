﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using Phase2WebAPI.Models;
using Phase2.API.Models;
using System.Web.Helpers;
using System.Data.Entity.Migrations;
using System.Web.Http.Description;

namespace Phase2.API.Controllers
{
    public class ApiKeyController : ApiController
    {
        private Phase2APIContext db = new Phase2APIContext();

        // change to "public string GetApiKey()" to see results when called. Do this by going to /api/ApiKey
        private static string GetApiKey()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }


        // TODO: add a && check for if an apikey already exist?
        // This method is called when putting /api/ApiKey.... into the url. How it knows what to get what we need,
        // I have no idea, but it does.
        // It calls the User Model (email, password, id, apikey) for reference.
        [HttpGet]
        public HttpResponseMessage GetUser([FromUri] User user)
        {

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);

            // Yay! this is the important part.
            // Pass the url's input (named 'user') to the IsValid function. If it returns true, then generate
            // and add an api key to that user.
            if (IsValid(user.Email, user.Password))
            {
                // This gets us our connection to the database
                var stud = db.Users.FirstOrDefault(u => u.Email.Equals(user.Email));
                // Generates an ApiKey
                var apikey = GetApiKey();
                // Puts api key into the database.
                stud.ApiKey = apikey;
                // Updates database
                db.Users.AddOrUpdate(stud);
                // Saves changes
                db.SaveChanges();

                // This basically returns the xml page with data in it.
                // type /api/ApiKey?email=admin&password=adminadmin  to see what I mean.
                // This method is a HttpResponseMessage which means it has to return a HttpResponseMessage. 
                // I don't know how to just disregard that, so I kept it like this.
                return response = Request.CreateResponse(HttpStatusCode.OK, stud);

                // I'd imagine putting a redirect function somehow here would be what we need,
                // atleast for the HTTP 200 page.
            }

            


            // at the top of my head, we should probably add an 'if else(email or password is incorrect){GoTo HTTP 403}else{GoTo HTTP 400}'
            return response;
        }

        // checks if a username (in this case, it'd be email) and password are valid in the database.
        // returns a true or false
        private bool IsValid(string userName, string password)
        {
            bool IsValid = false;

            var user = db.Users.FirstOrDefault(u => u.Email.Equals(userName));

            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                IsValid = true;
            }
            return IsValid;
        }
    }
}
