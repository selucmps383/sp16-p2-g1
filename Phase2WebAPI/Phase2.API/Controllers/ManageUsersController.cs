﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phase2WebAPI.Models;
using Phase2.API.Models;
using System.Web.Helpers;
using System.Net;
using System.Data.Entity.Migrations;

namespace Phase2.API.Controllers
{
    public class ManageUsersController : Controller
    {

        private Phase2APIContext db = new Phase2APIContext();

        // GET: ManageUsers
        public ActionResult UserManagement()
        {
            return View(db.Users.ToList());
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Phase2WebAPI.Models.User user)
        {
            bool userNameInUse = db.Users.Any(u => u.Email.Equals(user.Email));
            if (userNameInUse == true)
            {
                ModelState.AddModelError("", "Username in use.");


            }
            else
            {
                if(ModelState.IsValid)
                {
                    user.Password = Crypto.HashPassword(user.Password);
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("UserManagement");
                }
            }
            return View(user);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,Password")] Phase2WebAPI.Models.User user)
        {
          
                
            try
            {
                if (ModelState.IsValid)
                {
                    user.Password = Crypto.HashPassword(user.Password);
                    //input api key here
                    db.Users.AddOrUpdate(user);
                    db.SaveChanges();
                    return RedirectToAction("UserManagement");
                }
                return View(user);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

        }

        private bool IsValid(string userName, string password)
        {
            bool IsValid = false;

            var user = db.Users.FirstOrDefault(u => u.Email.Equals(userName));

            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                IsValid = true;
            }
            return IsValid;
        }

        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("UserManagement");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}