﻿function SPAController($scope, $routeParams, MyService) {

    //get store and cart from MyService
    $scope.store = MyService.store;
    $scope.cart = MyService.cart;

    //find products by id?
    if ($routeParams.productId != null) {

        $scope.product = $scope.store.getProducts($routeParams.productId);

    }
}