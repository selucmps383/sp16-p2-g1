﻿'use strict'

var SPA = angular.module('SPAstore', []).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.
        when('/store', {
            templateUrl: 'Index/SPA',
            //SPA route needs to be determined, this is just a guess...
            controller: SPAController
    });
        //no other routes necessary bc this is a SPA
    }]);

//store and shopping cart using data service
MyApp.factory("MyService", function () {
    var myStore = new store();
    var myCart = new shoppingCart("MyStore");

    //paypall???
    myCart.addCheckoutParameters("PayPal", "storeEmail@gmail.com");

    //return data obj w/store and cart
    return {

        store: StoreCart,  //change to myStore?
        cart: myCart

    };
});

