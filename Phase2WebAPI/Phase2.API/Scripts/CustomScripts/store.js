﻿function store() {

    //need to bind DB data to app!
    this.fakeProducts = [

        new product("prod1", "Socks", "001", 20),
        new product("prod2", "Shirts", "002", 20),
        new product("prod3", "Pants", "003", 20)

    ];

}

store.prototype.getProduct = function (id) {

    for (var i = 0; i < this.fakeProducts.length; i++) {

        if (this.fakeProducts[i].id == id)
            return this.fakeProducts[i];

    }
    return null;

}