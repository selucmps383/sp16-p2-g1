﻿function shoppingCart(cartName) {

    this.cartName = cartName;
    this.clearCart = false;
    this.checkoutParameters = {};
    this.items = [];
    this.loadItems();
    var self = this;
    $(window).unload(function () {

        if(se;f.clearCart){

            self.clearItems();

        }

        self.saveItems();
        self.clearCart = false;

    });

}


//use local storage to load and save items in cart

shoppingCart.prototype.loadItems = function(){

    var items = localStorage != null ? localStorage[this.cartName + "_items"] :
        null;
    if (items != null && JSON != null){

        try{

            var items = JSON.parse(items);
            for (var i = 0; i < items.length; i++){

                var item = items[i];

                if (item.id != null && item.name != null && item.price != null && item.quantity != null) {

                    item = new cartItem(item.id, item.name, item.price, item.quantity);
                    this.items.push(item);

                }

            }

        }

        catch (err){

            //catch errors while loading
            
        }


    }

}

shoppingCart.prototype.saveItems = function () {

    if(localStorage != null && JSON != null) {

        localStorage[this.cartName + "_items"] = JSON.stringify(this.items);

    }

}


//get total price of items in cart

shoppingCart.prototype.getTotalPrice = function(id) {

    var total = 0;
    for(var i = 0; i < this.items.length; i++){

        var item = this.items[i];
        if (id == null || item.is == id){

            total += this.toNumber(item.quantity * item.price);

        }

    }

    return total;

}

//get total price for all items in cart
shoppingCart.prototype.getTotalCount = function (id){

    var count = 0;
    for (var i = 0; i < this.items.length; i++){

        var item = this.items[i];
        if(id == null || item.id == id){

            count += this.toNumber(item.quantity);

        }

    }

    return count;

}


//clear items from cart

shoppingCart.prototype.clearItems = function(){

    this.items = [];
    this.saveItems();

}