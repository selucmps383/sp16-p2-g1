﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable.Serializers;
using System.Threading.Tasks;
using System.Net;
using RestSharp.Portable;
using Android.Views;
using Android.Content;

namespace Xamarin_Android_App
{
	[Activity (Label = "ProductActivity")]			
	public class ProductActivity : ListActivity
	{

		public string baseUrl = GlobalVariables.baseUrl;
		public string userApiKey = GlobalVariables.userApiKey;
		public int userId = GlobalVariables.userId;

		private ListView productsListView ;  
		private ArrayAdapter<ProductDTO> listAdapter ;
		public List<ProductDTO> ProductList;


		protected async override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.ProductLayout);


			ListView productsListView = FindViewById<ListView> (Resource.Id.productsListView);

			string url = "api/Products/";
			var json = await FetchProducts(url);

			ProductList = json.ToList();

			listAdapter = new ArrayAdapter<ProductDTO>(this, Resource.Layout.ProductLayout, ProductList);

			productsListView.SetAdapter(listAdapter);

		}


		public async Task<IQueryable<ProductDTO>> FetchProducts(string url)
		{
			RestClient client = new RestClient (baseUrl);
			var request = new RestSharp.Portable.RestRequest (baseUrl + url, Method.GET);
			var response = await client.Execute<IQueryable<ProductDTO>>(request);
			return response.Data;
		}


	}
}

