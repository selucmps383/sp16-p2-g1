﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable.Serializers;
using System.Threading.Tasks;
using System.Net;
using RestSharp.Portable;
using Android.Content;

namespace Xamarin_Android_App
{
    [Activity(Label = "Xamarin_Android_App", MainLauncher = true, Icon = "@drawable/icon")]
    public class LoginActivity : Activity
    {

		public string baseUrl = "http://147.174.182.224:53879/";
		public string userApiKey = "";
		public int userId = -1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.LoginLayout);

            EditText txtEmail = FindViewById<EditText>(Resource.Id.txtUserName);
            EditText txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
            Button btnLogin = FindViewById<Button>(Resource.Id.btnLogin);



            btnLogin.Click += async (sender, e) => {
				string url = "api/ApiKey?email=" + txtEmail.Text + "&password=" + txtPassword.Text;
				var json = await FetchApiKeyAndUserId(url);

				userApiKey = json.apiKey;
				userId = json.Id;

				GlobalVariables.baseUrl = baseUrl;
				GlobalVariables.userApiKey = userApiKey;
				GlobalVariables.userId = userId;

				if(userId != -1 && !string.IsNullOrEmpty(userApiKey))
				{
					var intent = new Intent(this, typeof(ProductActivity));
					StartActivity(intent);
				}
				else
				{
					throw new Exception("Invalid Login");
				}
            };
        }

		public async Task<UserDTO> FetchApiKeyAndUserId(string url)
        {
			RestClient client = new RestClient (baseUrl);
			var request = new RestSharp.Portable.RestRequest (baseUrl + url, Method.GET);
			var response = await client.Execute<UserDTO>(request);
			return response.Data;
        }

    }
}

