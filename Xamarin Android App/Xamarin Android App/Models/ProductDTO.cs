﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Xamarin_Android_App
{
	public class ProductDTO
	{
		public string Name { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime LastModifiedDate { get; set; }
		public decimal Price { get; set; }
		public int InventoryCount { get; set; }
		public int CategoryId { get; set; }
		public int ManufacturerId { get; set; }
		public IList<CategoryDTO> Category { get; set; }
		public IList<ManufacturerDTO> Manufacturer { get; set; }		

	}
}

