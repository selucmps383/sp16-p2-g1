﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Xamarin_Android_App
{
	public class ManufacturerDTO
	{
		public int ManufacturerId { get; set; }
		public string ManufacturerName { get; set; }
		public IList<ProductDTO> ProductDTO { get; set; }

		public ManufacturerDTO ()
		{
		}
	}
}

