﻿using System;

namespace Xamarin_Android_App
{
	public class UserDTO
	{
		public string email { get; set; }
		public string password { get; set; }
		public string apiKey { get; set; }
		public int Id { get; set; }
	}
}

