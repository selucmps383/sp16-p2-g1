﻿using System;

namespace Xamarin_Android_App
{
	public class GlobalVariables
	{
		public static string userApiKey { get; set; }
		public static int userId { get; set; }
		public static string baseUrl { get; set; }
	}
}

